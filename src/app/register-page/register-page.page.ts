import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.page.html',
  styleUrls: ['./register-page.page.scss'],
})
export class RegisterPagePage implements OnInit {

  vehicleType: string = ''

  constructor(private route: Router) { }

  ngOnInit() {
  }
  
  search () : void {
    this.route.navigate(['/situation-page'])
  }

}
