import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan1-page',
  templateUrl: './plan1-page.page.html',
  styleUrls: ['./plan1-page.page.scss'],
})
export class Plan1PagePage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  navToSuccess () {
    this.route.navigate(['/success'])
  }

}
