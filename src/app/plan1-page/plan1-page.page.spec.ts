import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Plan1PagePage } from './plan1-page.page';

describe('Plan1PagePage', () => {
  let component: Plan1PagePage;
  let fixture: ComponentFixture<Plan1PagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Plan1PagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Plan1PagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
