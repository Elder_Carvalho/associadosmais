import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Plan1PagePage } from './plan1-page.page';

const routes: Routes = [
  {
    path: '',
    component: Plan1PagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Plan1PagePage]
})
export class Plan1PagePageModule {}
