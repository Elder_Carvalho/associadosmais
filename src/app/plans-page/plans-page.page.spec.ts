import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlansPagePage } from './plans-page.page';

describe('PlansPagePage', () => {
  let component: PlansPagePage;
  let fixture: ComponentFixture<PlansPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlansPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlansPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
