import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plans-page',
  templateUrl: './plans-page.page.html',
  styleUrls: ['./plans-page.page.scss'],
})
export class PlansPagePage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navTo (page) {
    this.router.navigate([`/${page}`])
  }

}
