import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-situation-page',
  templateUrl: './situation-page.page.html',
  styleUrls: ['./situation-page.page.scss'],
})
export class SituationPagePage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  back () {
    history.back()
  }

  navTo (page) {
    this.route.navigate([`/${page}`])
  }
}
