import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituationPagePage } from './situation-page.page';

describe('SituationPagePage', () => {
  let component: SituationPagePage;
  let fixture: ComponentFixture<SituationPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituationPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituationPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
