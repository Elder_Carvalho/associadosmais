import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'register-page', loadChildren: './register-page/register-page.module#RegisterPagePageModule' },
  { path: 'situation-page', loadChildren: './situation-page/situation-page.module#SituationPagePageModule' },
  { path: 'plans-page', loadChildren: './plans-page/plans-page.module#PlansPagePageModule' },
  { path: 'plan1-page', loadChildren: './plan1-page/plan1-page.module#Plan1PagePageModule' },
  { path: 'plan2', loadChildren: './plan2/plan2.module#Plan2PageModule' },
  { path: 'success', loadChildren: './success/success.module#SuccessPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
