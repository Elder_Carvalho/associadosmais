import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Plan2Page } from './plan2.page';

describe('Plan2Page', () => {
  let component: Plan2Page;
  let fixture: ComponentFixture<Plan2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Plan2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Plan2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
