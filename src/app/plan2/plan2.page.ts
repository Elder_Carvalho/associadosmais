import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan2',
  templateUrl: './plan2.page.html',
  styleUrls: ['./plan2.page.scss'],
})
export class Plan2Page implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  navToSuccess () {
    this.route.navigate(['/success'])
  }

}
